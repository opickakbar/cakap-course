package com.technicaltest.cakap.course.service;

import com.technicaltest.cakap.course.dto.CourseDto;
import com.technicaltest.cakap.course.repository.CourseRepository;
import com.technicaltest.cakap.course.service.impl.CourseServiceImpl;
import com.technicaltest.common.Course;
import com.technicaltest.common.response.course.InsertCourseResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.redis.core.RedisTemplate;

public class CourseServiceImplTest {

    @InjectMocks
    private CourseServiceImpl courseService;

    @Mock
    private CourseRepository courseRepository;

    @Mock
    private RedisTemplate<String, Object> redisTemplate;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        Mockito.verifyNoMoreInteractions(courseRepository);
        Mockito.verifyNoMoreInteractions(redisTemplate);
    }

    @Test
    void insertCourse_whenAllValid_thenShouldSuccess () {
        Mockito.when(courseRepository.save(CourseDto.builder().build())).thenReturn(CourseDto.builder().build());
        InsertCourseResponse insertCourseResponse = courseService.insertCourse(Course.builder().build());
        Assertions.assertEquals(InsertCourseResponse.builder()
                .status(true)
                .message("Insert course success!")
                .build(), insertCourseResponse);
        Mockito.verify(courseRepository).save(CourseDto.builder().build());
    }


}
