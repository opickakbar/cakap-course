package com.technicaltest.cakap.course.util;

import com.technicaltest.common.Course;
import com.technicaltest.common.exception.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RequestValidationHelperTest {

    @Test
    void validateRequestAllValidTest () {
        Course insertCourse = Course.builder()
                .courseName("Test course")
                .author("Taufik Akbar")
                .description("Test description")
                .url("https://youtube.com")
                .build();
        RequestValidationHelper.validateRequest(insertCourse);
    }

    @Test
    void validateRequestMissingCourseNameTest () {
        Course insertCourse = Course.builder()
                .author("Taufik Akbar")
                .description("Test description")
                .url("https://youtube.com")
                .build();
        try {
            RequestValidationHelper.validateRequest(insertCourse);
        } catch (ValidationException exception) {
            Assertions.assertEquals("course_name is required", exception.getMessage());
        }
    }

    @Test
    void validateRequestMissingAuthorTest () {
        Course insertCourse = Course.builder()
                .courseName("Test course")
                .description("Test description")
                .url("https://youtube.com")
                .build();
        try {
            RequestValidationHelper.validateRequest(insertCourse);
        } catch (ValidationException exception) {
            Assertions.assertEquals("author is required", exception.getMessage());
        }
    }
}
