package com.technicaltest.cakap.course.controller;

import com.technicaltest.cakap.course.service.CourseService;
import com.technicaltest.common.Course;
import com.technicaltest.common.exception.AuthException;
import com.technicaltest.common.request.course.SearchCourseRequest;
import com.technicaltest.common.response.course.InsertCourseResponse;
import com.technicaltest.common.response.course.SearchCourseResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CourseControllerTest {

    @InjectMocks
    private CourseController courseController;

    @Mock
    private CourseService courseService;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        Mockito.verifyNoMoreInteractions(courseService);
    }

    @Test
    void searchCourse_whenAllValid_thenShouldReturnSuccess() {
        Mockito.when(courseService.searchCourse("TestingCourse")).thenReturn(SearchCourseResponse.builder().build());
        ResponseEntity<SearchCourseResponse> actual = courseController.searchCourse(SearchCourseRequest.builder()
                .search("TestingCourse")
                .build());
        Assertions.assertEquals(HttpStatus.OK, actual.getStatusCode());
        Assertions.assertEquals(SearchCourseResponse.builder().build(), actual.getBody());
        Mockito.verify(courseService).searchCourse("TestingCourse");
    }

    @Test
    void insertCourse_whenAllValid_thenShouldReturnSuccess() {
        Mockito.when(courseService.insertCourse(Course.builder()
                .courseName("TestCourse")
                .author("Taufik Akbar")
                .description("Test description")
                .url("https://youtube.com")
                .build())).thenReturn(InsertCourseResponse.builder().build());
        ResponseEntity<InsertCourseResponse> actual = courseController.insertCourse(Course.builder()
                .courseName("TestCourse")
                .author("Taufik Akbar")
                .description("Test description")
                .url("https://youtube.com")
                .build(), "admin");
        Assertions.assertEquals(HttpStatus.OK, actual.getStatusCode());
        Assertions.assertEquals(InsertCourseResponse.builder().build(), actual.getBody());
        Mockito.verify(courseService).insertCourse(Course.builder()
                .courseName("TestCourse")
                .author("Taufik Akbar")
                .description("Test description")
                .url("https://youtube.com")
                .build());
    }

    @Test
    void insertCourse_whenRoleIsMember_thenShouldReturnError() {
        try {
            ResponseEntity<InsertCourseResponse> actual = courseController.insertCourse(Course.builder()
                    .courseName("TestCourse")
                    .author("Taufik Akbar")
                    .description("Test description")
                    .url("https://youtube.com")
                    .build(), "member");
        } catch (AuthException exception) {
            Assertions.assertEquals("Unauthorized! This feature is only for admin", exception.getMessage());
        }
    }

}
