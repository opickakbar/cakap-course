package com.technicaltest.cakap.course.repository;

import com.technicaltest.cakap.course.dto.CourseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<CourseDto, Long> {

    @Query(value = "SELECT course FROM CourseDto course WHERE course.courseName LIKE %:courseName%")
    List<CourseDto> searchByCourseName(@Param("courseName") String courseName);
}
