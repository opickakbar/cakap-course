package com.technicaltest.cakap.course.service;

import com.technicaltest.common.Course;
import com.technicaltest.common.response.course.InsertCourseResponse;
import com.technicaltest.common.response.course.SearchCourseResponse;

public interface CourseService {
    InsertCourseResponse insertCourse (Course insertCourseRequest);
    SearchCourseResponse searchCourse (String courseName);
}
