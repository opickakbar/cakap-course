package com.technicaltest.cakap.course.service.impl;

import com.technicaltest.cakap.course.dto.CourseDto;
import com.technicaltest.cakap.course.repository.CourseRepository;
import com.technicaltest.cakap.course.service.CourseService;
import com.technicaltest.common.Course;
import com.technicaltest.common.response.ConfigResponse;
import com.technicaltest.common.response.course.InsertCourseResponse;
import com.technicaltest.common.response.course.SearchCourseResponse;
import com.technicaltest.common.util.JsonUtil;
import io.lettuce.core.RedisCommandExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Value("${redis.url.mapping.ttl}")
    private Integer ttl;

    @Override
    public InsertCourseResponse insertCourse(Course insertCourseRequest) {
        courseRepository.save(CourseDto.builder()
                .courseName(insertCourseRequest.getCourseName())
                .author(insertCourseRequest.getAuthor())
                .description(insertCourseRequest.getDescription())
                .url(insertCourseRequest.getUrl())
                .build());
        return InsertCourseResponse.builder()
                .status(true)
                .message("Insert course success!")
                .build();
    }

    @Override
    public SearchCourseResponse searchCourse(String courseName) {
        ConfigResponse configResponse = null;
        String redisSearchKey = "SEARCH_" + courseName;
        try {
            configResponse = (ConfigResponse) redisTemplate
                    .boundValueOps(redisSearchKey).get();
        } catch (RedisConnectionFailureException | RedisCommandExecutionException e) {
            log.error("Failed to connect to redis: {}", e.getMessage());
        }
        if (configResponse != null) {
            log.info("Get search data from redis, redisResponse = {}", configResponse);
            SearchCourseResponse searchCourseResponse = JsonUtil.toObject(configResponse.getValue(), SearchCourseResponse.class);
            return searchCourseResponse;
        }
        List<CourseDto> courseDtoList = courseRepository.searchByCourseName(courseName);
        log.info("Get search data from database, databaseResponse = {}", courseDtoList);
        SearchCourseResponse searchCourseResponse = constructSearchCourseResponse(courseDtoList);
        try {
            configResponse = new ConfigResponse(redisSearchKey, JsonUtil.toJsonString(searchCourseResponse));
            BoundValueOperations<String, Object> boundValueOperations = redisTemplate.boundValueOps(redisSearchKey);
            boundValueOperations.set(configResponse);
            boundValueOperations.expire(ttl, TimeUnit.MILLISECONDS);
        } catch (RedisConnectionFailureException | RedisCommandExecutionException e) {
            log.error("Failed to connect to redis: {}", e.getMessage());
        }
        return searchCourseResponse;
    }

    private SearchCourseResponse constructSearchCourseResponse (List<CourseDto> courseDtoList) {
        SearchCourseResponse searchCourseResponse = SearchCourseResponse.builder()
                .status(true)
                .message("Search course success")
                .build();
        List<Course> courseList = new ArrayList<>();
        for (CourseDto courseDto : courseDtoList) {
            courseList.add(Course.builder()
                    .courseName(courseDto.getCourseName())
                    .author(courseDto.getAuthor())
                    .description(courseDto.getDescription())
                    .url(courseDto.getUrl())
                    .build());
        }
        searchCourseResponse.setData(courseList);
        return searchCourseResponse;
    }
}
