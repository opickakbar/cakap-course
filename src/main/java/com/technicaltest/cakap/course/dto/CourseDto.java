package com.technicaltest.cakap.course.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = "courses")
@NoArgsConstructor
@AllArgsConstructor
public class CourseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "course_generator")
    @SequenceGenerator(
            name = "course_generator",
            sequenceName = "course_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column(name = "course_name")
    private String courseName;

    @Column(name = "author")
    private String author;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "url", columnDefinition = "text")
    private String url;

    @CreatedDate
    @Column(name = "created_at")
    @JsonIgnore
    private ZonedDateTime createdAt = ZonedDateTime.now();

    @LastModifiedDate
    @Column(name = "updated_at")
    @JsonIgnore
    private ZonedDateTime updatedAt = ZonedDateTime.now();
}
