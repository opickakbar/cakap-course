package com.technicaltest.cakap.course.util;

import com.technicaltest.common.Course;
import com.technicaltest.common.exception.ValidationException;

import java.util.Optional;

public class RequestValidationHelper {
    private RequestValidationHelper() {
    }

    public static void validateRequest(Course insertCourseRequest) {
        String courseName = Optional.ofNullable(insertCourseRequest)
                .map(Course::getCourseName)
                .orElseGet(String::new);
        String author = Optional.ofNullable(insertCourseRequest)
                .map(Course::getAuthor)
                .orElseGet(String::new);
        String description = Optional.ofNullable(insertCourseRequest)
                .map(Course::getDescription)
                .orElseGet(String::new);
        String url = Optional.ofNullable(insertCourseRequest)
                .map(Course::getUrl)
                .orElseGet(String::new);
        if (courseName.isEmpty()) {
            throw new ValidationException("course_name is required");
        } else if (author.isEmpty()) {
            throw new ValidationException("author is required");
        } else if (description.isEmpty()) {
            throw new ValidationException("description is required");
        } else if (url.isEmpty()) {
            throw new ValidationException("url is required");
        }
    }
}
