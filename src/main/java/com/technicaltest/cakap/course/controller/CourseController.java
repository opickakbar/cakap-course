package com.technicaltest.cakap.course.controller;

import com.technicaltest.cakap.course.service.CourseService;
import com.technicaltest.cakap.course.util.RequestValidationHelper;
import com.technicaltest.common.Course;
import com.technicaltest.common.exception.AuthException;
import com.technicaltest.common.request.course.SearchCourseRequest;
import com.technicaltest.common.response.course.InsertCourseResponse;
import com.technicaltest.common.response.course.SearchCourseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

    @Autowired
    private CourseService courseService;

    @PostMapping("/api/search")
    public ResponseEntity<SearchCourseResponse> searchCourse (@RequestBody SearchCourseRequest searchCourseRequest) {
        SearchCourseResponse searchCourseResponse = courseService.searchCourse(searchCourseRequest.getSearch());
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(searchCourseResponse);
    }

    @PostMapping("/api/insert")
    public ResponseEntity<InsertCourseResponse> insertCourse (@RequestBody Course insertCourseRequest, @RequestHeader("role") String role) {
        if (!role.equalsIgnoreCase("admin")) {
            throw new AuthException("Unauthorized! This feature is only for admin");
        }
        RequestValidationHelper.validateRequest(insertCourseRequest);
        InsertCourseResponse insertCourseResponse = courseService.insertCourse(insertCourseRequest);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(insertCourseResponse);
    }
}
